<!--llamada a la cabecera; logo, menú...-->
<?php
get_header();
?>

<a href="#" class="archivos_popup pum-trigger">archivos</a>
<!--contenido principal-->
<div id="main-content">
  <div class="container">
    <!--buscador-->
    <div class="buscador center" style="">
      <?php //echo do_shortcode('[searchandfilter fields="search,category,tipo-practica,nivel-educativo"]');
        //echo do_shortcode('[facetwp facet="example"]');
      ?>
      <?php //echo do_shortcode('[wpdreams_ajaxsearchlite]');
      //  echo do_shortcode('[wi_autosearch_suggest_form]');
      ?>

    <?php //echo get_search_form();


    ?>

    </div>
    <div id="content-area" class="clearfix">


      <?php //echo do_shortcode('[wpdreams_ajaxsearchlite]');
      //echo do_shortcode('[buscar_bbpp]');
      ?>



      <div id="left-area">
        <!--título general-->
        <div class="title_search_bp">
        <h1>Resultados de B&uacute;squeda:</h1>
        </div>
        <div class="linea_search_bp"></div>
        <?php //echo do_shortcode('[wpdreams_ajaxsearchlite]');
          echo do_shortcode('[search_bbpp]');
        ?>

        <div id="search_post_bp">
              <!--si hay post, entra en el bucle-->
              <?php if ( have_posts() ) : ?>
                <!--el loop-->
                <?php //var_dump(the_post());   ?>
                <?php while ( have_posts() ) : the_post(); ?>
                  <!-- recopilando info de cada post -->
                  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="border-bottom: 1px solid #eeeeee;margin-bottom: 18px;padding: 0 0 9px;">


                    <!-- contenido del post -->
                    <div class="entry-content" style="padding-bottom: 20px;height: 220px;">
                      <div class="search-thumb" style="float: left; margin-right: 20px;     width: 300px;
    height: 201px;">
                          <a href="<?php the_permalink();?>">
                        <?php the_post_thumbnail('medium', array( 'class'   => "search-thumb attachment-post-thumbnail")); ?>
                        </a>
                      </div>
                      <!-- título del post -->
                      <a href="<?php the_permalink();?>">
                      <h1 class="entry-title main_title" style="font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif;font-size: 21px;color: #000000!important;"><?php the_title(); ?></h1>
                        </a>

                        <a href="<?php the_permalink();?>">
                      <?php
                      /*
                      if ( has_post_thumbnail() ) { // check if the post Thumbnail
                        the_post_thumbnail();
                      } else {
                        //your default img
                      }
                      */
                      the_excerpt();
                      ?>
                </a>

                    </div> <!-- fin contenido post -->
                  </article> <!-- fin info de cada post -->
                <?php endwhile; ?><!-- fin del loop -->
                <!-- si no hay post de búsqueda -->
              <?php else: ?>

                <p>No hemos encontrado resultados para su b&uacute;squeda. Encontrar&aacute; un &iacute;ndice con todas las entradas de este blog en:</p>
                <div id="enlace-todas-las-entradas">
                  <h1> <a href="https://laprogramaciondehoy.com/indice-entradas-blog-lph/">>> Ver todas las entradas del Blog Lph</a></h1>
                </div>
              <?php endif;?>
              </div>

    </div> <!-- termina contenido derecha -->

    <?php get_sidebar(); ?><!-- barra lateral -->
  </div> <!-- fin de div contentarea -->
  </div> <!-- fin de div container -->
</div> <!-- fin de div contenido principal -->

<style>
/********************************************/
/* Resultado de busqueda */
.title_search_bp h1{
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;
  font-size: 40px !important;
  color: #7f6ba5!important;
}

.linea_search_bp{
  border-top-color: #eab2dd;
  border-top-width: 6px;
  border-top-style: solid;
    width: 20%;
    padding-bottom: 30px
}

.entry-content p{
  color: #575756!important;
  font-size: 18px;
  overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    line-height: 20px;
    max-height: 120px;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
    margin-bottom: 8px;
}
.entry-content a:hover{
  text-decoration: underline;
  color: #575756!important;
}

</style>


<?php

//echo do_shortcode('[facetwp facet="my_facet_name"]');
//echo facetwp_display( 'facet', 'busqueda_avanzada' ); ?>
<?php //echo facetwp_display( 'template', 'busqueda_avanzada' ); ?>
<?php //echo facetwp_display( 'pager' ); ?>

<?php get_footer(); ?><!-- llamada al pie de página -->
