<!--llamada a la cabecera; logo, menú...-->
<?php
get_header();
?>


<!--contenido principal-->
<div id="main-content">
  <div class="container">
    <!--buscador-->
    <div class="buscador center" style="">
      <?php echo do_shortcode('[searchandfilter fields="search,category,tipo-practica,nivel-educativo"]');
        //echo do_shortcode('[wpbsearch]');
      ?>
      <?php //echo do_shortcode('[wpdreams_ajaxsearchlite]');
      //  echo do_shortcode('[wi_autosearch_suggest_form]');
      ?>
    <?php //echo get_search_form(); ?>

    </div>
    <div id="content-area" class="clearfix">


      <?php //echo do_shortcode('[wpdreams_ajaxsearchlite]');
      //echo do_shortcode('[buscar_bbpp]');
      ?>



      <div id="left-area">
        <!--título general-->
        <div class="title_search_bp">
        <h1>Resultados de B&uacute;squeda:</h1>
        </div>
        <div class="linea_search_bp"></div>
      <!--
        <span class="et_pb_fullwidth_header_subhead"> Encontrar&aacute; m&aacute;s informaci&oacute;n en:
        -->
          <!--las categorías del blog-->
          <?php
          /*
          $categories = get_categories();
          foreach ( $categories as $category ) {
            if (!each($categories)){
            */
          ?>
          <!--
          <h3 class="h2" style="font-family: 'Montserrat', sans-serif;">
            <a href="<?php //echo esc_url( get_category_link(get_cat_ID($category->name)))?>" > <?php echo esc_html( $category->name )?> </a>
          </h3>
          -->
          <?php
          /*
            }else {
            */
          ?>
            <!--
          <a href="<?php //echo esc_url( get_category_link(get_cat_ID($category->name)))?>"> <?php echo esc_html( $category->name )?> >></a>
          -->
        <?php
        /*
            }
          }
          */
        ?>
      </span>

      <!--si hay post, entra en el bucle-->
      <?php if ( have_posts() ) : ?>
        <!--el loop-->
        <?php while ( have_posts() ) : the_post(); ?>
          <!-- recopilando info de cada post -->
          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="border-bottom: 1px solid #eeeeee;margin-bottom: 18px;padding: 0 0 9px;">
            <!-- título del post -->
            <h1 class="entry-title main_title"><?php the_title(); ?></h1>

            <!-- contenido del post -->
            <div class="entry-content">
              <div class="search-thumb">
                <?php the_post_thumbnail('thumbnail', array( 'class'   => "search-thumb attachment-post-thumbnail")); ?>
              </div>
              <?php
              /*
              if ( has_post_thumbnail() ) { // check if the post Thumbnail
                the_post_thumbnail();
              } else {
                //your default img
              }
              */
              the_excerpt();
              ?>
              <a href="<?php the_permalink();?>">Leer más...</a>

            </div> <!-- fin contenido post -->
          </article> <!-- fin info de cada post -->
        <?php endwhile; ?><!-- fin del loop -->
        <!-- si no hay post de búsqueda -->
      <?php else: ?>

        <p>No hemos encontrado resultados para su b&uacute;squeda. Encontrar&aacute; un &iacute;ndice con todas las entradas de este blog en:</p>
        <div id="enlace-todas-las-entradas">
          <h1> <a href="https://laprogramaciondehoy.com/indice-entradas-blog-lph/">>> Ver todas las entradas del Blog Lph</a></h1>
        </div>
      <?php endif;?>
    </div> <!-- termina contenido derecha -->

    <?php get_sidebar(); ?><!-- barra lateral -->
  </div> <!-- fin de div contentarea -->
  </div> <!-- fin de div container -->
</div> <!-- fin de div contenido principal -->

<style>
/********************************************/
/* Resultado de busqueda */
.title_search_bp h1{
  font-family: 'Gotham Rounded Medium',Helvetica,Arial,Lucida,sans-serif!important;
  font-size: 40px !important;
  color: #7f6ba5!important;
}

.linea_search_bp{
  border-top-color: #eab2dd;
  border-top-width: 6px;
  border-top-style: solid;
    width: 20%;
    padding-bottom: 30px
}

</style>

<?php get_footer(); ?><!-- llamada al pie de página -->
