<?php
//Obtenemos los 3 primeros post
$args = Array(
  'cat' => 59,
    "orderby"=>"ID",
    "order"=>"DESC",
    "posts_per_page"=>4
);
$the_query = new WP_Query($args);
$dato_bp1=array();
if ( $the_query->have_posts() ) :
  while ( $the_query->have_posts() ) : $the_query->the_post();
      $post = get_post();
      $dato_bp1[]=$post->ID;
  endwhile;
else :
  _e( 'Sorry, no posts matched your criteria.' );
endif;

//listamos los post que no se encuentran en la primera lista
$args_filtro = Array(
    'cat' => 59,
    "orderby"=>"ID",
    "order"=>"DESC",
    "posts_per_page"=>3,
    'post__not_in' =>  $dato_bp1
);

$the_query2 = new WP_Query($args_filtro);
 ?>




  <div class="">
  <div class="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light  et_pb_text_align_left">


  </div> <!-- .et_pb_column -->
  </div> <!-- .et_pb_row --><div class="et_pb_row_bp et_pb_row_9 et_pb_row_4col">


    <!--si hay post, entra en el bucle-->
    <?php if ( $the_query2->have_posts() ) : ?>
      <!--el loop-->
      <?php
      $dato_bp="";
      //unset($post->post_title);
      while ( $the_query2->have_posts() ) : $the_query2->the_post(); ?>
    <div class="et_pb_column et_pb_column_1_4 et_pb_column_15  et_pb_css_mix_blend_mode_passthrough">

      <div class="et_pb_module et_pb_blurb et_pb_blurb_3 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
        <div class="et_pb_blurb_content">
          <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap_bp">
            <!--
            <img src="http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/agenda-foto1.jpg" srcset="http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/agenda-foto1.jpg 350w, http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/agenda-foto1-300x200.jpg 300w" sizes="(max-width: 350px) 100vw, 350px" class="et-waypoint et_pb_animation_top et-animated"></span></div>

          --><?php
          the_post_thumbnail('medium', array( 'class'   => "search-thumb attachment-post-thumbnail"));
          //the_post_thumbnail('medium', array( 'sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 500px' ));

          ?>
          <div class="et_pb_blurb_container">
            <h4 class="et_pb_module_header_bp"><span style="font-family: 'calibri bold';">
              <?php
              //the_title();

              $getlength = strlen($post->post_title);
                $thelength = 40;
                //$d=substr($post->post_title, 0, $thelength);
                $dato_bp=mb_substr($post->post_title,0,$thelength,'UTF-8');
                echo $dato_bp;
                if ($getlength > $thelength) echo "...";


                if (strlen($post->post_title) > 70) {
                  echo substr(the_title($before = '', $after = '', FALSE), 0, 70) . '...';
                } else {
                  the_title();
                }

                //$excerpt1= the_title(); echo substr($excerpt1, 0, 40);
              ?></span></h4>
            <div class="et_pb_blurb_description" style="font-family: 'calibri';">
              <p><?php //the_excerpt();
              ?>
                  <?php
                  //echo substr(strip_tags($post->post_content), 0, 110);

$excerpt= get_the_excerpt(); echo substr($excerpt, 0, 115);
                  ?>
              <a href="#"><strong><a href="<?php the_permalink();?>" style="float: left;">Leer más...</a></strong></a>
              </p>
            </div>
          </div>
        </div> <!-- .et_pb_blurb_content -->
      </div> <!-- .et_pb_blurb -->
    </div> <!-- .et_pb_column -->
    </div> <!-- .et_pb_row -->
  <?php endwhile; ?><!-- fin del loop -->
  <!-- si no hay post de búsqueda -->
<?php else: ?>

<?php endif;?>

</div>

<style>

.et_pb_main_blurb_image {
    line-height: 1;
}
.et_pb_blurb_3.et_pb_blurb {
    background-color:
    #ffffff;
    padding-top: 16px !important;
    padding-right: 15px !important;
    padding-bottom: 16px !important;
    padding-left: 15px !important;
}

.et_pb_module_header_bp{
    font-family: 'Calibri Bold',Helvetica,Arial,Lucida,sans-serif;
    /*text-transform: uppercase;*/
    color:#000000 !important;
}
.et_pb_blurb.et_pb_text_align_left .et_pb_blurb_content .et_pb_blurb_container {
    text-align: left;
}
.et_pb_image_wrap_bp img {
    max-width: 195px;
    height: 110px;
    margin-bottom: 20px;
}

.et_pb_row_bp {

    position: relative;
    width: 100%;
    max-width: 1080px;
    margin: auto;
        margin-top: auto;

}

</style>
