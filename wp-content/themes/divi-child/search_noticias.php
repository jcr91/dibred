<!--llamada a la cabecera; logo, menú...-->
<?php
get_header();
?>
<?php
//Obtenemos los 3 primeros post
$args = Array(
  'cat' => 59,
    "orderby"=>"ID",
    "order"=>"DESC",
    "posts_per_page"=>1
);
$the_query = new WP_Query($args);
$dato_bp1=array();
if ( $the_query->have_posts() ) :
  while ( $the_query->have_posts() ) : $the_query->the_post();
      $post = get_post();
      $dato_bp1[]=$post->ID;
  endwhile;
else :
  _e( 'Sorry, no posts matched your criteria.' );
endif;

//listamos los post que no se encuentran en la primera lista
$args_filtro = Array(
    'cat' => 59,
    "orderby"=>"ID",
    "order"=>"DESC",
    "posts_per_page"=>4,
    'post__not_in' =>  $dato_bp1
);

$the_query2 = new WP_Query($args_filtro);
 ?>



<div class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">
  <div class="et_pb_row et_pb_row_8">
  <div class="et_pb_column et_pb_column_4_4 et_pb_column_14  et_pb_css_mix_blend_mode_passthrough et-last-child">
    <div class="et_pb_module et_pb_text et_pb_text_15 et_pb_bg_layout_light  et_pb_text_align_left">
      <div class="et_pb_text_inner"><h1>Noticias</h1></div>
    </div> <!-- .et_pb_text --><div class="et_pb_module et_pb_divider et_pb_divider_3 et_pb_divider_position_ et_pb_space"><div class="et_pb_divider_internal"></div></div>
  </div> <!-- .et_pb_column -->
  </div> <!-- .et_pb_row -->
  <div class="et_pb_row et_pb_row_9 et_pb_row_4col">


    <!--si hay post, entra en el bucle-->
    <?php if ( $the_query2->have_posts() ) : ?>
      <!--el loop-->
      <?php while ( $the_query2->have_posts() ) : $the_query2->the_post(); ?>
    <div class="et_pb_column et_pb_column_1_4 et_pb_column_15  et_pb_css_mix_blend_mode_passthrough">

      <div class="et_pb_module et_pb_blurb et_pb_blurb_3 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
        <div class="et_pb_blurb_content">
          <div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap">
            <!--
            <img src="http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/agenda-foto1.jpg" srcset="http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/agenda-foto1.jpg 350w, http://10.200.8.76/~observatorio/wordpress/wp-content/uploads/2019/10/agenda-foto1-300x200.jpg 300w" sizes="(max-width: 350px) 100vw, 350px" class="et-waypoint et_pb_animation_top et-animated"></span></div>

          --><?php
          the_post_thumbnail('medium', array( 'class'   => "search-thumb attachment-post-thumbnail"));
          //the_post_thumbnail('medium', array( 'sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 500px' ));

          ?>
          <div class="et_pb_blurb_container">
            <h4 class="et_pb_module_header"><span>
              <?php
                //$post_title=the_title();
                //$title = the_title('','',FALSE); echo substr($title, 0, 40);

                //echo  $post->post_title;

                /*$thetitle = $post->post_title; /* or you can use get_the_title() */

                $getlength = strlen($post->post_title);
                $thelength = 40;
                //$d=substr($post->post_title, 0, $thelength);
                echo mb_substr($post->post_title,0,$thelength,'UTF-8');
                if ($getlength > $thelength) echo "...";

              ?>
            </span></h4>
            <div class="et_pb_blurb_description">
              <p><?php //the_excerpt();
              ?>
                  <?php echo substr(strip_tags($post->post_content), 0, 110);?>
              <br>
              <a href="#"><strong><a href="<?php the_permalink();?>">Leer más...</a></strong></a>
              </p>
            </div>
          </div>
        </div> <!-- .et_pb_blurb_content -->
      </div> <!-- .et_pb_blurb -->
    </div> <!-- .et_pb_column -->
    </div> <!-- .et_pb_row -->
  <?php endwhile; ?><!-- fin del loop -->
  <!-- si no hay post de búsqueda -->
<?php else: ?>

<?php endif;?>

</div>
</div>

<style>
.et_pb_main_blurb_image {
    line-height: 1;
}
.et_pb_blurb_3.et_pb_blurb {
    background-color:
    #ffffff;
    padding-top: 16px !important;
    padding-right: 15px !important;
    padding-bottom: 16px !important;
    padding-left: 15px !important;
}
.et_pb_blurb_3.et_pb_blurb h4, .et_pb_blurb_3.et_pb_blurb h4 a, .et_pb_blurb_3.et_pb_blurb h1.et_pb_module_header, .et_pb_blurb_3.et_pb_blurb h1.et_pb_module_header a, .et_pb_blurb_3.et_pb_blurb h2.et_pb_module_header, .et_pb_blurb_3.et_pb_blurb h2.et_pb_module_header a, .et_pb_blurb_3.et_pb_blurb h3.et_pb_module_header, .et_pb_blurb_3.et_pb_blurb h3.et_pb_module_header a, .et_pb_blurb_3.et_pb_blurb h5.et_pb_module_header, .et_pb_blurb_3.et_pb_blurb h5.et_pb_module_header a, .et_pb_blurb_3.et_pb_blurb h6.et_pb_module_header, .et_pb_blurb_3.et_pb_blurb h6.et_pb_module_header a {
    font-family: 'Calibri Bold',Helvetica,Arial,Lucida,sans-serif;
    text-transform: uppercase;
    color:#000000 !important;

}
.et_pb_blurb.et_pb_text_align_left .et_pb_blurb_content .et_pb_blurb_container {
    text-align: left;
}
.et_pb_image_wrap img {
    max-width: 195px;
    height: 110px;
    margin-bottom: 20px;
}

</style>
<?php get_footer(); ?><!-- llamada al pie de página -->
s
