<?php

get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>

<h1>Resultados de su B&uacute;squeda:</h1>
 <span class="et_pb_fullwidth_header_subhead"> Encontrar&aacute; m&aacute;s informaci&oacute;n en:
       <?php $categories = get_categories();
    foreach ( $categories as $category ) {
        if (!each($categories)){?>
   <a href="<?php echo esc_url( get_category_link(get_cat_ID($category->name)))?>"> <?php echo esc_html( $category->name )?> </a>
        <?php }
                       else {?>
                          <a href="<?php echo esc_url( get_category_link(get_cat_ID($category->name)))?>"> <?php echo esc_html( $category->name )?> >></a>
        <?php }
               } ?>
 </span>


<div class="buscador center"><?php echo get_search_form(); ?></div>

<?php if ( have_posts() ) : ?>


			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php if ( ! $is_page_builder_used ) : ?>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
					$thumb = '';

					$width = (int) apply_filters( 'et_pb_index_blog_image_width', 1080 );

					$height = (int) apply_filters( 'et_pb_index_blog_image_height', 675 );
					$classtext = 'et_featured_image';
					$titletext = get_the_title();
					$thumbnail = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
					$thumb = $thumbnail["thumb"];

					if ( 'on' === et_get_option( 'divi_page_thumbnails', 'false' ) && '' !== $thumb )
						print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
				?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
					/*
						the_content();


							*/
							the_excerpt();

							if ( ! $is_page_builder_used )
								wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'Divi' ), 'after' => '</div>' ) );
					?>
					<a href="<?php the_permalink();?>">Leer más...</a>
					</div> <!-- .entry-content -->

				<?php
					if ( ! $is_page_builder_used && comments_open() && 'on' === et_get_option( 'divi_show_pagescomments', 'false' ) ) comments_template( '', true );
				?>

				</article> <!-- .et_pb_post -->

			<?php endwhile; ?>

			<!--Termina el bucle y añadimos el nuevo código else-->
		<?php else: ?>
		  <p>No hemos encontrado resultados para su b&uacute;squeda. Encontrar&aacute; un &iacute;ndice con todas las entradas de este blog en:</p>
		  <div id="enlace-todas-las-entradas">
		      <h1> <a href="https://laprogramaciondehoy.com/indice-entradas-blog-lph/">>> Ver todas las entradas del Blog Lph</a></h1>
		  </div>
		<?php endif;?>
<!--Fin del if else-->


<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php

get_footer();
