<?php
/*
 * Agrega el atributo rel="noopener noreferrer" a todos las etiquetas a que contengas el atributo 
 */

add_action( 'wp_enqueue_scripts', 'ps_add_attr' );
add_action( 'admin_enqueue_scripts', 'ps_add_attr',1 );

function ps_add_attr()
{
    // Register the script like this for a plugin:
    wp_register_script( 'custom-script', plugins_url( '../public/js/parche.js', __FILE__ ), array( 'jquery','jquery-ui-core'),'20190919',true);
    // or
    // Register the script like this for a theme:
    wp_register_script( 'custom-script', get_template_directory_uri() . '../public/js/parche.js', array( 'jquery','jquery-ui-core' ),'20190919',true);
 
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'custom-script' );
}


