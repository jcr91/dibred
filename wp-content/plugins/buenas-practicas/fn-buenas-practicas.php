<?php
/*
Plugin Name: Buenas Practicas
Plugin URI:
Description: Agrega Custom Post Types al sitio Buenas Practicas
Version:     1.0
Author:     walter
Author URI:
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

add_action( 'init', 'crear_post_type_buenas_practicas', 0 );

function crear_post_type_buenas_practicas() {

// Etiquetas para el Post Type
$labels = array(
'name'                => _x( 'Buenas Prácticas', 'Post Type General Name', 'buena-practica' ),
'singular_name'       => _x( 'Buena Práctica', 'Post Type Singular Name', 'buena-practica' ),
'menu_name'           => __( 'Buenas Prácticas', 'buena-practica' ),
'parent_item_colon'   => __( 'Buena Práctica Padre', 'buena-practica' ),
'all_items'           => __( 'Todas las Buenas Prácticas', 'buena-practica' ),
'view_item'           => __( 'Ver Buena Práctica', 'buena-practica' ),
'add_new_item'        => __( 'Agregar Nueva Buena Práctica', 'buena-practica' ),
'add_new'             => __( 'Agregar Nueva Buena Práctica', 'buena-practica' ),
'edit_item'           => __( 'Editar Buena Práctica', 'buena-practica' ),
'update_item'         => __( 'Actualizar Buena Práctica', 'buena-practica' ),
'search_items'        => __( 'Buscar Buena Práctica', 'buena-practica' ),
'not_found'           => __( 'No encontrado', 'buena-practica' ),
'not_found_in_trash'  => __( 'No encontrado en la papelera', 'buena-practica' ),
);

// Otras opciones para el post type

$args = array(
'label'               => __( 'buena_practica', 'buena-practica' ),
'description'         => __( 'Buena Práctica news and reviews', 'buena-practica' ),
'labels'              => $labels,
// Todo lo que soporta este post type
'supports'            => array( 'title', '', '', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
/* Un Post Type hierarchical es como las paginas y puede tener padres e hijos.
* Uno sin hierarchical es como los posts
*/
'taxonomies'            => array(),
'hierarchical'        => false,
'public'              => true,
'show_ui'             => true,
'show_in_menu'        => true,
'show_in_nav_menus'   => true,
'show_in_admin_bar'   => true,
'menu_position'       => 5,
'menu_icon'           => 'dashicons-admin-page',
'can_export'          => true,
'has_archive'         => true,
'exclude_from_search' => false,
'publicly_queryable'  => true,
'capability_type'     => 'post',
);

// Por ultimo registramos el post type
register_post_type( 'buena_practica', $args );

}

function tipo_practica() {
		register_taxonomy(
			'tipo-practica',
			'buena_practica',
			array(
				'label' => __( 'Tipo de Práctica' ),
				'rewrite' => array( 'slug' => 'person' ),
	      'hierarchical' => true,
			)
		);
	}
	add_action( 'init', 'tipo_practica' );

	function nivel_educativo() {
			register_taxonomy(
				'nivel-educativo',
				'buena_practica',
				array(
					'label' => __( 'Nivel Educativo' ),
					'rewrite' => array( 'slug' => 'person' ),
		      'hierarchical' => true,
				)
			);
		}
		add_action( 'init', 'nivel_educativo' );

	add_shortcode('bp_listar_buenas_practicas', 'listar_buenas_practicas');
	 function listar_buenas_practicas() {

		 return 'prueba';

	 }
