<?php
/*
  Plugin Name: Custom Registration
  Plugin URI: http://code.tutsplus.com
  Description: Updates user rating based on number of posts.
  Version: 1.0
  Author: Agbonghama Collins
  Author URI: http://tech4sky.com
 */


//var_dump ($resultados);
include_once(ABSPATH . 'wp-includes/pluggable.php');
include_once(ABSPATH . 'wp-includes/class-phpass.php');
include_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

require_once plugin_dir_path(__FILE__) . 'includes/cr_functions.php';

register_activation_hook( __FILE__, 'cr_db_plugin' );


add_filter( 'edit_profile_url', 'modify_profile_url_wpse_94075', 10, 3 );

/**
 * http://core.trac.wordpress.org/browser/tags/3.5.1/wp-includes/link-template.php#L2284
 *
 * @param string $scheme The scheme to use.
 * Default is 'admin'. 'http' or 'https' can be passed to force those schemes.
*/
function modify_profile_url_wpse_94075( $url, $user_id, $scheme )
{
    // Makes the link to http://example.com/custom-profile
    $url = site_url( '/wp-admin/admin.php?page=my_menu_slug' );
    return $url;
}
/*
add_action( 'admin_menu', 'oaf_create_admin_menu');

function oaf_create_admin_menu() {
  add_menu_page ( 'User', 'User', 'manage_options', 'oaf_create_admin_menu_plugin', 'custom_registration_function', 'dashicons-megaphone' );
}
*/

function cr_add_mydashboard_sub_menu_page(){
  add_submenu_page(
  "users.php",
  "Edit User",
  "Edit User",
  "manage_options",
  "my_menu_slug",
  "custom_registration_function"
  );
}

add_action('admin_menu','cr_add_mydashboard_sub_menu_page');

/*
 * Remove items from admin submenu
 */
function cr_remove_submenu_items() {
    remove_submenu_page( 'users.php', 'profile.php' );        // Dashboard > Home
}
add_action( 'admin_init', 'cr_remove_submenu_items' );




add_action('wp_before_admin_bar_render', 'ya_do_it_admin_bar_remove', 0);

function custom_registration_function() {
    if (isset($_POST['submit'])) {
        registration_validation(
        $_POST['username'],
        $_POST['password'],
        $_POST['email'],
        $_POST['website'],
        $_POST['fname'],
        $_POST['lname'],
        $_POST['nickname'],
        $_POST['bio']
		);

		  // sanitize user form input
        global $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
        $username	= 	sanitize_user($_POST['username']);
        $password 	= 	esc_attr($_POST['password']);
        $email 		= 	sanitize_email($_POST['email']);
        $website 	= 	esc_url($_POST['website']);
        $first_name = 	sanitize_text_field($_POST['fname']);
        $last_name 	= 	sanitize_text_field($_POST['lname']);
        $nickname 	= 	sanitize_text_field($_POST['nickname']);
        $bio 		= 	esc_textarea($_POST['bio']);

		// call @function complete_registration to create the user
		// only when no WP_error is found
        complete_registration(
        $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $nickname,
        $bio
		);
    }

    registration_form(
    	 $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $nickname,
        $bio
		);
}

function registration_form( $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio ) {
global $cu;
$cu = wp_get_current_user();

  echo '
	<div id="wpwrap" id="profile-page">
	<h1 class="wp-heading-inline">
		Perfil</h1>
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
	<br>
	<h2>Opciones personales</h2>

	<table class="form-table" role="presentation">
	<tbody>
	<tr class="user-email-wrap">
	<th>
		<label for="username">Nombre de usuario <strong>*</strong></label>
	</th>
	<td>
	<input type="text" name="username" value="' . (esc_attr( $cu->user_login )) . '" class="regular-text" disabled="disabled">
	</td>
	</tr>
	<tr class="user-email-wrap">
	<th>
	<label for="password">contraseña <strong>*</strong></label>
	</th>
	<td>
	<div class="input-group">
		<span class="password_id">
		<input type="password" id="password" rel="gp" name="password" value="' . (esc_attr( $cu->user_pass )) . '" class="regular-text login-field  login-field-password" data-size="32" data-character-set="a-z,A-Z,0-9,#">
        </span>
		<span class="input-group-btn"><button type="button" class="btn btn-default getNewPass"><span class="fa fa-refresh"></span>Generar contraseña</button></span>
     </div>
	</td>
	</tr>
	<tr class="user-email-wrap">
	<th>
	<label for="email">Correo (requerido)<strong>*</strong></label>
	</th>
	<td>
	<input type="text" name="email" value="' . (esc_attr( $cu->user_email )) . '" class="regular-text">
	</td>
	</tr>
	<tr class="user-email-wrap">
	<th>
	<label for="website">Sitio Web</label>
	</th>
	<td>
	<input type="text" name="website" value="' . (esc_attr( $cu->user_url )) . '" class="regular-text">
	</td>
	</tr>
	<tr class="user-email-wrap">
	<th>
	<label for="firstname">Nombre</label>
	</th>
	<td>
	<input type="text" name="fname" value="' . (esc_attr( $cu->user_firstname )) . '" class="regular-text">
	</td>
	</tr>
	<tr class="user-email-wrap">
	<th>
	<label for="website">Apellidos</label>
	</th>
	<td>
	<input type="text" name="lname" value="' . (esc_attr( $cu->user_firstname )) . '" class="regular-text">
	</td>
	</tr>
	<tr class="user-email-wrap">
	<th>
	<label for="nickname">Alias (requerido)</label>
	</th>
	<td>
	<input type="text" name="nickname" value="' . (esc_attr( $cu->nickname )) . '" class="regular-text">
	</td>
	</tr>
	<tr class="user-email-wrap">
	<th>
	<label for="bio">Información biográfica</label>
	</th>
	<td>
	<textarea name="bio" rows="5" cols="50">' . (esc_attr( $cu->user_firstname )) . '</textarea>
	</td>
	<th>
	</tr>
	<tr class="user-email-wrap">

	</th>
	<td>
	<input type="submit" name="submit" value="Actualizar perfil"/>
	</td>
	</tr>
	</tbody>
	</table>
	</form>

	</div>


	';
}

function registration_validation( $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio )  {
    global $reg_errors,$wpdb;
    $reg_errors = new WP_Error;

	if (!is_null($password)) {

		if ( strtolower( $username ) === strtolower( $password ) ) {

			$reg_errors->add( 'my_distinct_user_pass', 'Username and password must be different' );

		}

		if ( ! preg_match( '/[0-9]/', $password ) ) {
			$reg_errors->add( 'my_pass_numeric', 'La contraseña debe tener al menos 1 carácter numérico' );
		}

		if ( ! preg_match( '/[a-z]/', $password ) ) {
			$reg_errors->add( 'my_pass_lowercase', 'La contraseña debe tener al menos 1 carácter en minúscula' );
		}

		if ( ! preg_match( '/[A-Z]/', $password ) ) {
			$reg_errors->add( 'my_pass_uppercase','La contraseña debe tener al menos 1 carácter en mayúscula');
		}

		if (empty( $password ) || empty( $email)  || empty($nickname) ) {
			$reg_errors->add('field', 'Falta el campo del formulario requerido');
		}

		if ( strlen( $password ) < 8 ) {
			$reg_errors->add('password', 'La longitud de la contraseña debe ser mayor que 8');
		}
    
		if ( !is_email( $email ) ) {
			$reg_errors->add('email_invalid', 'El correo no es válido');
		}
    
	/*	if ( email_exists( $email ) ) {
			$reg_errors->add('email', 'Correo electrónico ya en uso');
		} */

		if ( !empty( $website ) ) {
			if ( !filter_var($website, FILTER_VALIDATE_URL) ) {
				$reg_errors->add('website', 'El sitio web no es una URL válida');
			}
		}

		if ( !empty( $password ) ) {

			$rc_result = $wpdb->get_results( "SELECT id,user_pass,user_id FROM cr_pass_users" );

			foreach ( $rc_result as $rc_v )
			{
					$encpass = cr_openCypher('decrypt',$rc_v->user_pass);
					if($password==$encpass){

						$reg_errors->add('password', 'Reutilización de contraseña, Por favor ingresar otra contrasaña');
						break;

					}
			}

		}

		if ( is_wp_error( $reg_errors ) ) {

			foreach ( $reg_errors->get_error_messages() as $error ) {
				echo '<div class="error"><p>';
				echo $error . '<br/>';
				echo '</p></div>';
			}
		}

	}
}

function complete_registration() {

  global $reg_errors, $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio,$cu,$wpdb;
	$cu = wp_get_current_user();

    if ( count($reg_errors->get_error_messages()) < 1 ) {
        $userdata = array(
		    'ID' => $cu->ID,
        /*'user_login'	=> 	$username,*/
        'user_email' 	=> 	$email,
        'user_pass' 	=> 	$password,
        'user_url' 		=> 	$website,
        'first_name' 	=> 	$first_name,
        'last_name' 	=> 	$last_name,
        'nickname' 		=> 	$nickname,
        'description' 	=> 	$bio,
		);

		$returnValue=wp_update_user($userdata );

		$wpdb->insert( 'cr_pass_users',
			array(
				  'user_pass' =>cr_openCypher('encrypt',$password),
				  'user_id' => $cu->ID
				)

			);
		echo '<div id="message" class="updated notice is-dismissible"><p><strong>El perfil ha sido actualizado.</p></strong></div> ';
	}
}

// Register a new shortcode: [cr_custom_registration]
add_shortcode('cr_custom_registration', 'custom_registration_shortcode');

// The callback function that will replace [book]
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}

//add_action( 'wp_enqueue_scripts', 'cr_add_attr' );
add_action( 'admin_enqueue_scripts', 'cr_add_attr',1 );

function cr_add_attr()
{
  wp_register_style( 'cr_namespace', plugins_url( '/css/example.wink.css', __FILE__ ) );
	wp_register_style( 'cr_namespace2', plugins_url( '/css/generar_pass.css', __FILE__ ) );

	wp_enqueue_style( 'cr_namespace' );
  wp_enqueue_style( 'cr_namespace2' );

  wp_register_script( 'cr_custom-script', plugins_url( '/js/hideShowPassword.min.js', __FILE__ ), array( 'jquery','jquery-ui-core'),'20190919',true);
  wp_register_script( 'cr_custom-script', get_template_directory_uri() . '/js/hideShowPassword.min.js', array( 'jquery','jquery-ui-core' ),'20190919',true);

  wp_register_script( 'cr_custom-script2', plugins_url( '/js/hideShowPassword.js', __FILE__ ), array( 'jquery','jquery-ui-core'),'20190919',true);
  wp_register_script( 'cr_custom-script2', get_template_directory_uri() . '/js/hideShowPassword.js', array( 'jquery','jquery-ui-core' ),'20190919',true);

  wp_register_script( 'cr_custom-script3', plugins_url( '/js/generar_pass.js', __FILE__ ), array( 'jquery','jquery-ui-core'),'20190919',true);
  wp_register_script( 'cr_custom-script3', get_template_directory_uri() . '/js/generar_pass.js', array( 'jquery','jquery-ui-core' ),'20190919',true);

  // For either a plugin or a theme, you can then enqueue the script:
  wp_enqueue_script( 'cr_custom-script' );
	wp_enqueue_script( 'cr_custom-script2' );
	wp_enqueue_script( 'cr_custom-script3' );

}
