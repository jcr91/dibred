jQuery(document).ready(function(){
  /**
   * Validacion del formulario Iniciar session
   */
   jQuery('#swpm_user_name').on('change invalid', function() {
      var campotexto = jQuery(this).get(0);
      campotexto.setCustomValidity('');

      if (!campotexto.validity.valid) {
        campotexto.setCustomValidity('El campo Numero de Documento es obligatorio');
      }
    });

    jQuery('#swpm_password').on('change invalid', function() {
       var campotexto = jQuery(this).get(0);
       campotexto.setCustomValidity('');

       if (!campotexto.validity.valid) {
         campotexto.setCustomValidity('El campo Contraseña es obligatorio');
       }
     });

  /**
    * Validacion del formulario de registro
    */
    jQuery('.swpm-registration-fec_nac-row').hide();
    jQuery('.swpm-registration-tipo_doc-row').hide();

    jQuery('.swpm-registration-subperfil-row').hide();

    jQuery('#grupo_1').hide();
    jQuery('.swpm-registration-id_nivel-row').hide();
    jQuery('.swpm-registration-id_dre-row').hide();
    jQuery('.swpm-registration-id_ugel-row').hide();
    jQuery('.swpm-registration-id_institucion-row').hide();
    jQuery('.swpm-registration-id_carrera-row').hide();
    jQuery('.swpm-registration-codigo_modular-row').hide();
    jQuery('.swpm-registration-centro_laboral-row').hide();
    jQuery('.swpm-registration-id_ministerio-row').hide();
    jQuery('.swpm-registration-id_cargo-row').hide();

/*
?page_id=51617:1 An invalid form control with name='id_centro' is not focusable.
?page_id=51617:1 An invalid form control with name='id_departamento' is not focusable.
?page_id=51617:1 An invalid form control with name='id_provincia' is not focusable.
?page_id=51617:1 An invalid form control with name='id_distrito' is not focusable.

?page_id=51617:1 An invalid form control with name='id_nivel' is not focusable.
?page_id=51617:1 An invalid form control with name='id_dre' is not focusable.
?page_id=51617:1 An invalid form control with name='id_ugel' is not focusable.
?page_id=51617:1 An invalid form control with name='id_institucion' is not focusable.
?page_id=51617:1 An invalid form control with name='id_carrera' is not focusable.
?page_id=51617:1 An invalid form control with name='codigo_modular' is not focusable.
?page_id=51617:1 An invalid form control with name='centro_laboral' is not focusable.
?page_id=51617:1 An invalid form control with name='id_ministerio' is not focusable.
?page_id=51617:1 An invalid form control with name='id_cargo' is not focusable.
?page_id=51617:1 An invalid form control with name='fec_nac' is not focusable.
?page_id=51617:1 An invalid form control with name='tipo_doc' is not focusable.
*/


    jQuery("#id_perfil").change(function(){
      var id = jQuery(this).find(":selected").val();
      console.log(id);
      if (id!='4'){
        jQuery('#grupo_1').show();
        jQuery('#id_centro').attr('required', '');
        jQuery('#id_departamento').attr('required', '');
        jQuery('#id_provincia').attr('required', '');
        jQuery('#id_distrito').attr('required', '');
      }else{
        jQuery('#grupo_1').hide();
        jQuery('#id_centro').removeAttr('required');
        jQuery('#id_departamento').removeAttr('required');
        jQuery('#id_provincia').removeAttr('required');
        jQuery('#id_distrito').removeAttr('required');
      }


      switch (id) {
        case '1':/* Investigador */
          jQuery('.swpm-registration-centro_laboral-row').show();

          jQuery('.swpm-registration-id_nivel-row').hide();
          jQuery('.swpm-registration-id_institucion-row').hide();
          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-id_ministerio-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('#centro_laboral').attr('required', '');
          break;
        case '2':/* Estudiante */
          jQuery('.swpm-registration-id_nivel-row').show();
          jQuery('.swpm-registration-id_institucion-row').show();
          jQuery('.swpm-registration-id_carrera-row').show();

          jQuery('.swpm-registration-codigo_modular-row').hide();
          jQuery('.swpm-registration-id_dre-row').hide();
          jQuery('.swpm-registration-id_ugel-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();
          jQuery('.swpm-registration-id_ministerio-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('#id_nivel').attr('required', '');
          jQuery('#id_institucion').attr('required', '');
          jQuery('#id_carrera').attr('required', '');
          break;
        case '3':/* Docente */
          jQuery('.swpm-registration-id_nivel-row').show();
          jQuery('.swpm-registration-id_institucion-row').show();
          jQuery('.swpm-registration-codigo_modular-row').show();
          jQuery('.swpm-registration-id_dre-row').show();
          jQuery('.swpm-registration-id_ugel-row').show();

          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();
          jQuery('.swpm-registration-id_ministerio-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('#id_nivel').attr('required', '');
          jQuery('#id_institucion').attr('required', '');
          jQuery('#codigo_modular').attr('required', '');
          jQuery('#id_dre').attr('required', '');
          jQuery('#id_ugel').attr('required', '');
          break;
        case '4':/* Funcionario */
          jQuery('.swpm-registration-id_institucion-row').show();
          jQuery('.swpm-registration-codigo_modular-row').show();
          jQuery('.swpm-registration-id_dre-row').show();
          jQuery('.swpm-registration-id_ugel-row').show();
          jQuery('.swpm-registration-id_ministerio-row').show();
          jQuery('.swpm-registration-id_cargo-row').show();

          jQuery('#grupo_1').hide();
          jQuery('.swpm-registration-id_nivel-row').hide();
          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();

          jQuery('#id_institucion').attr('required', '');
          jQuery('#codigo_modular').attr('required', '');
          jQuery('#id_dre').attr('required', '');
          jQuery('#id_ugel').attr('required', '');
          jQuery('#id_ministerio').attr('required', '');
          jQuery('#id_cargo').attr('required', '');
          break;
        case '5':/* Otros */
          jQuery('.swpm-registration-centro_laboral-row').show();

          jQuery('.swpm-registration-id_nivel-row').hide();
          jQuery('.swpm-registration-id_institucion-row').hide()
          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-codigo_modular-row').hide();
          jQuery('.swpm-registration-id_dre-row').hide();
          jQuery('.swpm-registration-id_ugel-row').hide();
          jQuery('.swpm-registration-id_ministerio-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('#centro_laboral').attr('required', '');
          break;
        default:
          jQuery('#grupo_1').hide();
          jQuery('.swpm-registration-id_nivel-row').hide();
          jQuery('.swpm-registration-id_dre-row').hide();
          jQuery('.swpm-registration-id_ugel-row').hide();
          jQuery('.swpm-registration-id_institucion-row').hide();
          jQuery('.swpm-registration-id_carrera-row').hide();
          jQuery('.swpm-registration-codigo_modular-row').hide();
          jQuery('.swpm-registration-centro_laboral-row').hide();
          jQuery('.swpm-registration-id_ministerio-row').hide();
          jQuery('.swpm-registration-id_cargo-row').hide();

          jQuery('#id_nivel').removeAttr('required');
          jQuery('#id_dre').removeAttr('required');
          jQuery('#id_ugel').removeAttr('required');
          jQuery('#id_institucion').removeAttr('required');
          jQuery('#id_carrera').removeAttr('required');
          jQuery('#codigo_modular').removeAttr('required');
          jQuery('#centro_laboral').removeAttr('required');
          jQuery('#id_ministerio').removeAttr('required');
          jQuery('#id_cargo').removeAttr('required');
          jQuery('#fec_nac').removeAttr('required');
          jQuery('#tipo_doc').removeAttr('required');

      }

    /* Investigador */
    /*
      if (id=='1'){
        jQuery('.swpm-registration-centro_laboral-row').show();
      }else{
        jQuery('.swpm-registration-centro_laboral-row').hide();
        //jQuery('#id_distrito').removeAttr('required');
      }
      */

      /* Estudiante */
      /*
      if (id=='2'){
        jQuery('.swpm-registration-id_nivel-row').show();
        jQuery('.swpm-registration-id_institucion-row').show();
        jQuery('.swpm-registration-id_carrera-row').show();

        jQuery('#id_dre').removeAttr('required');
        jQuery('#id_ugel').removeAttr('required');
        jQuery('#fec_nac').removeAttr('required');
        jQuery('#tipo_doc').removeAttr('required');
        jQuery('#id_subperfil').removeAttr('required');
        jQuery('#codigo_modular').removeAttr('required');
        jQuery('#centro_laboral').removeAttr('required');
        jQuery('#id_ministerio').removeAttr('required');
        jQuery('#id_cargo').removeAttr('required');

      }else{
        jQuery('.swpm-registration-id_nivel-row').hide();
        jQuery('.swpm-registration-id_institucion-row').hide();
        jQuery('.swpm-registration-id_carrera-row').hide();



      }
      */

      /* Docente */
      /*
      if (id=='3'){
        jQuery('.swpm-registration-id_nivel-row').show();
        jQuery('.swpm-registration-id_institucion-row').show();
        jQuery('.swpm-registration-codigo_modular-row').show();
        jQuery('.swpm-registration-id_dre-row').show();
        jQuery('.swpm-registration-id_ugel-row').show();
      }else{
        jQuery('.swpm-registration-id_nivel-row').hide();
        jQuery('.swpm-registration-id_institucion-row').hide();
        jQuery('.swpm-registration-codigo_modular-row').hide();
        jQuery('.swpm-registration-id_dre-row').hide();
        jQuery('.swpm-registration-id_ugel-row').hide();

      }
*/
      /* Funcionario */
      /*
      if (id=='4'){

        jQuery('.swpm-registration-id_institucion-row').show();
        jQuery('.swpm-registration-codigo_modular-row').show();
        jQuery('.swpm-registration-id_dre-row').show();
        jQuery('.swpm-registration-id_ugel-row').show();
        jQuery('.swpm-registration-id_ministerio-row').show();
        jQuery('.swpm-registration-id_cargo-row').show();
      }else{
        jQuery('.swpm-registration-id_institucion-row').hide();
        jQuery('.swpm-registration-codigo_modular-row').hide();
        jQuery('.swpm-registration-id_dre-row').hide();
        jQuery('.swpm-registration-id_ugel-row').hide();
        jQuery('.swpm-registration-id_ministerio-row').hide();
        jQuery('.swpm-registration-id_cargo-row').hide();

      }
      */

      /* Otros */
      /*
      if (id=='5'){
        jQuery('.swpm-registration-centro_laboral-row').show();
      }else{
        jQuery('.swpm-registration-centro_laboral-row').hide();
      }
      */
    });



    jQuery("#id_perfil").change(function(){

    var id = jQuery(this).find(":selected").val();
    //console.log(id);
    jQuery.getJSON("/~observatorio/wordpress/wp-admin/admin-ajax.php",{
        action: 'get_subperfil',
        perfil: id
      },
        function(data) { //Procesamiento de los datos de vuelta
          console.log(JSON.stringify(data));

          if(!jQuery.isEmptyObject(data)){
            console.log(jQuery.isEmptyObject(data));
            jQuery('.swpm-registration-subperfil-row').show();
          }else{
            jQuery('.swpm-registration-subperfil-row').hide();
          }

          jQuery('#id_subperfil').empty();
          var $select = jQuery('#id_subperfil');

          //alert(options);
          $select.append('<option value=""></option>');
          jQuery.each(data, function(i,n) {
            //console.log(i);
            $select.append('<option value=' + i + '>' + n + '</option>');
          });



        });
  });
/*
  jQuery("#ubigeo").keyup(function(a){
  	if(jQuery(this).val().length!=6 ){
  		jQuery("#bp_mensaje").html("El ubigeo tiene que contener 6 digitos");
  	}else{
      bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}
  });
  jQuery("#ubigeo").blur(function(a){
    if(jQuery(this).val().length!=6 ){
  		jQuery("#bp_mensaje").html("El ubigeo tiene que contener 6 digitos");
  	}else{
      bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}
  });
  */

  jQuery("#ubigeo").keyup(function(a){
  	if(jQuery(this).val().length==6 && jQuery("#user_name").val().length==8){
  		bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  		jQuery("#bp_mensaje").html("");
  	}
  });

  jQuery("#user_name").keyup(function(a){
  	if(jQuery(this).val().length==8 && jQuery("#ubigeo").val().length==6){
  	   bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  	jQuery("#bp_mensaje").html("");
  	}
  });


  jQuery("#user_name").blur(function(a){
  	if(jQuery(this).val().length==8 && jQuery("#ubigeo").val().length==6){
  	   bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  	   jQuery("#bp_mensaje").html("");
  	}
  });

  jQuery("#ubigeo").blur(function(a){
  	if(jQuery(this).val().length==6 && jQuery("#user_name").val().length==8){
  	   bpValidarDatos(jQuery("#user_name").val(),jQuery("#ubigeo").val());
  	}else{
  	   jQuery("#bp_mensaje").html("");
  	}
  });

  //FUNCION PARA VALIDAR DATOS DEL definitions
  function bpValidarDatos(valor1,valor2){

    jQuery.getJSON("/~observatorio/wordpress/wp-admin/admin-ajax.php",{
      action: 'get_datos_dni',
      nro_doc: valor1,
      ubigeo:valor2
    },
    function(data) { //Procesamiento de los datos de vuelta
      console.log(JSON.stringify(data));
      var bp_msg="";
      if(data.msg=="0"){
			  bp_msg="El DNI es Invalido";
		  }else{
        if(data.msg=="1"){
            //bp_msg=data.nombres;
            //bp_msg="Datos correctos"
            bp_msg=data.nombres+' '+data.apellidos;
        }else{
            bp_msg="Los datos no son correctos";
        }
      }
	     //jQuery("#bp_mensaje").html(bp_msg)
      jQuery("#bp_mensaje").html(bp_msg);
       jQuery('input[name="first_name"]:hidden').val(data.nombres);
       jQuery('input[name="last_name"]:hidden').val(data.apellidos);
      /*jQuery('#first_name').val(data.nombres);
      jQuery('#last_name').val(data.apellidos);*/
    });
  }


/* Validacion para el registro*/



jQuery("#user_name").keydown(function(e)
{
    var key = e.charCode || e.keyCode || 0;
    // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
    // home, end, period, and numpad decimal
    return (
        key == 8 ||
        key == 9 ||
        key == 46 ||
        key == 110 ||
        key == 190 ||
        (key >= 35 && key <= 40) ||
        (key >= 48 && key <= 57) ||
        (key >= 96 && key <= 105));
});

/* Termina*/
//jQuery("#nro_doc").validarNumero();
  jQuery("#nro_doc").keydown(function(e)
  {
      var key = e.charCode || e.keyCode || 0;
      // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal
      return (
          key == 8 ||
          key == 9 ||
          key == 46 ||
          key == 110 ||
          key == 190 ||
          (key >= 35 && key <= 40) ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105));
  });



  jQuery("#ubigeo").keydown(function(e)
  {
      var key = e.charCode || e.keyCode || 0;
      // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
      // home, end, period, and numpad decimal
      return (
          key == 8 ||
          key == 9 ||
          key == 46 ||
          key == 110 ||
          key == 190 ||
          (key >= 35 && key <= 40) ||
          (key >= 48 && key <= 57) ||
          (key >= 96 && key <= 105));
  });







//jQuery("#ubigeo").validarNumero();
/*
jQuery.fn.validarNumero =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};
*/
  /*
    jQuery('#pido_ajax').click(function() {
      data1value='1';
      data2value='2';
        jQuery.getJSON("/wordpress_prueba/wp-admin/admin-ajax.php",
                  {action: 'get_treatments', data1: data1value, data2: data2value},
          function(data) { //Procesamiento de los datos de vuelta }

          })
        })
*/

var filter = jQuery('#filter');
//console.log(filter);

bpSearch(filter);


jQuery('#filter').submit(function(){


  var filter = jQuery('#filter');
  //console.log("prueba");
  bpSearch(filter);
  return false;
});

console.log(filter.attr('action'));
//FUNCION PARA VALIDAR DATOS DEL definitions
function bpSearch(filter){
  jQuery.ajax({
      url:filter.attr('action'),
      data:filter.serialize(), // form data
      type:filter.attr('method'), // POST
      beforeSend:function(xhr){
          filter.find('button').text('PROCESANDO...'); // changing the button label
      },
      success:function(data){
          filter.find('button').text('BUSCAR'); // changing the button label back
          jQuery('#response').html(data); // insert data
      }
  });

}


jQuery('#brand').on('change', function() {
  //alert( this.value );
  jQuery("#filter").submit();
  var filter = jQuery('#filter');
  bpSearch(filter);


});

jQuery('#size').on('change', function() {
  //alert( this.value );
  jQuery("#filter").submit();
  var filter = jQuery('#filter');
  bpSearch(filter);

});

/*********************************************/
var filter_bp = jQuery('#filter_bp');
//console.log(filter);
//bpSearch2(filter_bp);

function bpSearch2(filter_bp){
    //var filter_bp = jQuery('#filter_bp');
    jQuery.ajax({
        url:filter_bp.attr('action'),
        data:filter_bp.serialize(), // form data
        type:filter_bp.attr('method'), // POST
        beforeSend:function(xhr){
            filter_bp.find('button').text('LOAD...'); // changing the button label
        },
        success:function(data){
            filter_bp.find('button').text('BUSCAR'); // changing the button label back
            console.log(data);
            jQuery('#response_search').html(data); // insert data
        }
    });
	};
  jQuery('#brand2').on('change', function() {
    //alert( this.value );
    //jQuery("#filter_bp").submit();
    var filter_bp = jQuery('#filter_bp');
    bpSearch2(filter_bp);
    jQuery("#search_post_bp").hide();
  });

  jQuery('#size2').on('change', function() {
    //alert( this.value );
    //jQuery("#filter_bp").submit();
    var filter_bp = jQuery('#filter_bp');
    bpSearch2(filter_bp);
    jQuery("#search_post_bp").hide();
  });

jQuery("#search_bp").keyup(function(a){
  var filter_bp = jQuery('#filter_bp');
  bpSearch2(filter_bp);
  jQuery("#search_post_bp").hide();
});


jQuery("#search_bp").blur(function(a){
  var filter_bp = jQuery('#filter_bp');
  bpSearch2(filter_bp);
  jQuery("#search_post_bp").hide();
});

jQuery('#category').on('change', function() {
  //alert( this.value );
  //jQuery("#filter_bp").submit();
  var filter_bp = jQuery('#filter_bp');
  bpSearch2(filter_bp);

    jQuery("#search_post_bp").hide();
});

/**
  * eventos para listar imagens buena practica
  */
  jQuery(".bp_listar_evidencias").hide();

  jQuery( ".bp_button_evidencia" ).click(function() {
    //jQuery(".bp_listar_evidencias").show();
    $(".bp_listar_evidencias").toggle();
    return false;
  });

/*
var search_bp = jQuery('#filter_bp');
console.log(search_bp);
*/
/*
bpSearch2(search_bp);


jQuery('#filter_bp').submit(function(){
  var search_bp = jQuery('#filter_bp');
  //console.log(search_bp);
  bpSearch2(search_bp);
  return false;
});
*/
//FUNCION PARA VALIDAR DATOS DEL definitions
/*
console.log(search_bp.attr('action'));
function bpSearch2(search_bp){
  jQuery.ajax({
      url:search_bp.attr('action'),
      data:search_bp.serialize(), // form data
      type:search_bp.attr('method'), // POST
      beforeSend:function(xhr){
          search_bp.find('button').text('PROCESANDO...'); // changing the button label
      },
      success:function(data){
          search_bp.find('button').text('BUSCAR'); // changing the button label back
          console.log(data);
          jQuery('#response_search').html(data); // insert data
      }
  });

}

jQuery('#brand2').on('change', function() {
  //alert( this.value );
  jQuery("#filter_bp").submit();
  var search_bp = jQuery('#filter_bp');
  bpSearch2(search_bp);
});

jQuery('#size2').on('change', function() {
  //alert( this.value );
  jQuery("#filter_bp").submit();
  var search_bp = jQuery('#filter_bp');
  bpSearch2(search_bp);
});

*/
});
