<?php
SimpleWpMembership::enqueue_validation_scripts(array('ajaxEmailCall' => array('extraData' => '&action=swpm_validate_email&member_id=' . filter_input(INPUT_GET, 'member_id'))));
$settings = SwpmSettings::get_instance();
$force_strong_pass = $settings->get_value('force-strong-passwords');
if (!empty($force_strong_pass)) {
    $pass_class = "validate[required,custom[strongPass],minSize[8]]";
} else {
    $pass_class = "";
}
?>
<div class="swpm-registration-widget-form">
    <form id="swpm-registration-form" class="swpm-validate-form" name="swpm-registration-form" method="post" action="">
        <input type ="hidden" name="level_identifier" value="<?php echo $level_identifier ?>" />
        <table>
          <tr class="swpm-registration-id_perfil-row">
              <td><label for="id_perfil"><?php echo SwpmUtils::_('Perfil'); ?></label></td>
              <td><select id="id_perfil" name="id_perfil"><?php echo SwpmMiscUtils::get_perfil_dropdown($id_perfil) ?></select></td>
          </tr>
          <tr class="swpm-registration-id_subperfil-row">
              <td><label for="id_subperfil"><?php echo SwpmUtils::_('Sub Perfil'); ?></label></td>
              <td><select id="id_subperfil" name="id_subperfil"><?php echo SwpmMiscUtils::get_subperfil_dropdown($id_subperfil) ?></select></td>
          </tr>

            <tr class="swpm-registration-username-row">
                <td><label for="user_name"><?php echo SwpmUtils::_('Número de documento') ?></label></td>
                <td><input type="text" id="user_name" class="validate[required,custom[noapostrophe],custom[SWPMUserName],minSize[4],ajax[ajaxUserCall]]" value="<?php echo esc_attr($user_name); ?>" size="50" name="user_name" maxlength="8" /></td>
            </tr>

            <tr class="swpm-registration-ubigeo-row">
              <td><label for="ubigeo"><?php echo SwpmUtils::_('Ubigeo'); ?></label></td>
              <td><input type="text" id="ubigeo" value="<?php echo esc_attr($ubigeo); ?>" size="40" name="ubigeo" maxlength="6" /></td>
            </tr>

            <tr class="swpm-profile-bp_mensaje-row">
              <td>

              </td>
                <td>
                  <label for="bp_mensaje"><div id="bp_mensaje"></div></label>
                </td>

            </tr>

            <tr class="swpm-registration-firstname-row">
                <td><label for="first_name"><?php echo SwpmUtils::_('Nombres') ?></label></td>
                <td><input type="text" id="first_name" value="<?php echo esc_attr($first_name); ?>" size="50" name="first_name" readonly/></td>
            </tr>
            <tr class="swpm-registration-lastname-row">
                <td><label for="last_name"><?php echo SwpmUtils::_('Apellidos') ?></label></td>
                <td><input type="text" id="last_name" value="<?php echo esc_attr($last_name); ?>" size="50" name="last_name" readonly /></td>
            </tr>

            <tr class="swpm-registration-email-row">
                <td><label for="email"><?php echo SwpmUtils::_('Email') ?></label></td>
                <td><input type="text" autocomplete="off" id="email" class="validate[required,custom[email],ajax[ajaxEmailCall]]" value="<?php echo esc_attr($email); ?>" size="50" name="email" /></td>
            </tr>
            <tr class="swpm-registration-password-row">
                <td><label for="password"><?php echo SwpmUtils::_('Password') ?></label></td>
                <td><input type="password" autocomplete="off" id="password" class="<?php echo $pass_class; ?>" value="" size="50" name="password" /></td>
            </tr>
            <tr class="swpm-registration-password-retype-row">
                <td><label for="password_re"><?php echo SwpmUtils::_('Repeat Password') ?></label></td>
                <td><input type="password" autocomplete="off" id="password_re" value="" size="50" name="password_re" /></td>
            </tr>

            <tr class="swpm-registration-phone-row">
                <td><label for="phone"><?php echo SwpmUtils::_('Número de Celular'); ?></label></td>
                <td><input type="text" id="phone" value="<?php echo $phone; ?>" size="50" name="phone" /></td>
            </tr>

            <tr class="swpm-registration-id_centro-row">
                <td><label for="id_centro"><?php echo SwpmUtils::_('Centro Laboral'); ?></label></td>
                <td><select id="id_centro" name="id_centro"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select></td>
            </tr>

            <tr class="swpm-registration-nivel-row">
                <td><label for="id_nivel"><?php echo SwpmUtils::_('Nivel Educativo'); ?></label></td>
                <td><select id="id_nivel" name="id_nivel"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select></td>
            </tr>

            <tr class="swpm-registration-dre-row">
                <td><label for="id_dre"><?php echo SwpmUtils::_('DRE'); ?></label></td>
                <td><select id="id_dre" name="id_dre"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select></td>
            </tr>

            <tr class="swpm-registration-ugel-row">
                <td><label for="id_ugel"><?php echo SwpmUtils::_('UGEL'); ?></label></td>
                <td><select id="id_ugel" name="id_ugel"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select></td>
            </tr>

            <tr class="swpm-registration-ugel-row">
                <td><label for="id_ugel"><?php echo SwpmUtils::_('UGEL'); ?></label></td>
                <td><select id="id_ugel" name="id_ugel"><?php echo SwpmMiscUtils::get_centro_dropdown($id_centro) ?></select></td>
            </tr>

            <tr class="swpm-registration-id_departamento-row">
                <td><label for="id_departamento"><?php echo SwpmUtils::_('Departamento'); ?></label></td>
                <td><select id="id_departamento" name="id_departamento"><?php echo SwpmMiscUtils::get_region_dropdown($id_region) ?></select></td>
            </tr>
            <tr class="swpm-registration-id_provincia-row">
                <td><label for="id_provincia"><?php echo SwpmUtils::_('Provincia'); ?></label></td>
                <td><select id="id_provincia" name="id_provincia"><?php echo SwpmMiscUtils::get_provincia_dropdown($id_provincia) ?></select></td>
            </tr>
            <tr class="swpm-registration-id_distrito-row">
                <td><label for="id_distrito"><?php echo SwpmUtils::_('Distrito'); ?></label></td>
                <td><select id="id_distrito" name="id_distrito"><?php echo SwpmMiscUtils::get_distrito_dropdown($id_distrito) ?></select></td>
            </tr>

            <tr class="swpm-registration-id_institucion-row">
                <td><label for="id_institucion"><?php echo SwpmUtils::_('Institución Educativa'); ?></label></td>
                <td><select id="id_institucion" name="id_institucion"><?php echo SwpmMiscUtils::get_institucion_dropdown($id_institucion) ?></select></td>
            </tr>

            <!--
            <tr class="swpm-registration-firstname-row">
                <td><label for="first_name"><?php //echo SwpmUtils::_('First Name') ?></label></td>
                <td><input type="text" id="first_name" value="<?php //echo esc_attr($first_name); ?>" size="50" name="first_name" /></td>
            </tr>
            <tr class="swpm-registration-lastname-row">
                <td><label for="last_name"><?php //echo SwpmUtils::_('Last Name') ?></label></td>
                <td><input type="text" id="last_name" value="<?php //echo esc_attr($last_name); ?>" size="50" name="last_name" /></td>
            </tr>
          -->
            <tr class="swpm-registration-membership-level-row">
                <td><label for="membership_level"><?php echo SwpmUtils::_('Membership Level') ?></label></td>
                <td>
                    <?php
                    echo $membership_level_alias; //Show the level name in the form.
                    //Add the input fields for the level data.
                    echo '<input type="hidden" value="' . $membership_level . '" size="50" name="membership_level" id="membership_level" />';
                    //Add the level input verification data.
                    $swpm_p_key = get_option('swpm_private_key_one');
                    if (empty($swpm_p_key)) {
                        $swpm_p_key = uniqid('', true);
                        update_option('swpm_private_key_one', $swpm_p_key);
                    }
                    $swpm_level_hash = md5($swpm_p_key . '|' . $membership_level); //level hash
                    echo '<input type="hidden" name="swpm_level_hash" value="' . $swpm_level_hash . '" />';
                    ?>
                </td>
            </tr>
            <?php
            //check if we need to display Terms and Conditions checkbox
            $terms_enabled = $settings->get_value('enable-terms-and-conditions');
            if (!empty($terms_enabled)) {
                $terms_page_url = $settings->get_value('terms-and-conditions-page-url');
                ?>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <label><input type="checkbox" id="accept_terms" name="accept_terms" class="validate[required]" value="1"> <?php echo SwpmUtils::_('I accept the ') ?> <a href="<?php echo $terms_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Terms and Conditions') ?></a></label>
                    </td>
                </tr>
                <?php
            }
            //check if we need to display Privacy Policy checkbox
            $pp_enabled = $settings->get_value('enable-privacy-policy');
            if (!empty($pp_enabled)) {
                $pp_page_url = $settings->get_value('privacy-policy-page-url');
                ?>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <label><input type="checkbox" id="accept_pt" name="accept_pp" class="validate[required]" value="1"> <?php echo SwpmUtils::_('I agree to the ') ?> <a href="<?php echo $pp_page_url; ?>" target="_blank"><?php echo SwpmUtils::_('Privacy Policy') ?></a></label>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>

        <div class="swpm-before-registration-submit-section" align="center"><?php echo apply_filters('swpm_before_registration_submit_button', ''); ?></div>

        <div class="swpm-registration-submit-section" align="center">
            <input type="submit" value="<?php echo SwpmUtils::_('Register') ?>" class="swpm-registration-submit" name="swpm_registration_submit" />
        </div>

        <input type="hidden" name="action" value="custom_posts" />

    </form>
</div>
